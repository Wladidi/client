import React, {useContext, useEffect, useState} from 'react';
import authContext from '../context/auth/authContext';
import estacionamientoContext from '../context/estacionamientos/estacionamientoContext';
import msgContext from '../context/mensajes/msgContext';
import {vLength} from '../helpers';

const useStacionamiento = () => {
  const msjContext = useContext(msgContext);
  const {crearMensaje} = msjContext;
  const autContext = useContext(authContext);
  const estContext = useContext(estacionamientoContext);
  const {
    mensajeCreacion,
    agregarEstacionamiento,
    editarEstacionamiento,
  } = estContext;
  const {usuario} = autContext;
  const [estacionamiento, setEstacionamiento] = useState({
    comuna: usuario.comuna,
    direccion: usuario.direccion,
    horaInicio: Date.now(),
    horaFin: Date.now(),
    telefono: '',
    precio: '',
    descripcion: '',
    tipoEstacionamiento: 1,
  });

  const checkValues = () => {
    if (
      vLength(estacionamiento.direccion) ||
      vLength(estacionamiento.telefono) ||
      vLength(estacionamiento.precio)
    ) {
      crearMensaje('Debe ingresar todos los campos', 'vEstacionamiento');
      return;
    }
    if (!/^[0-9]+$/.test(estacionamiento.telefono)) {
      crearMensaje(
        'Debe ingresar un numero sin puntos ni guiones',
        'vEstacionamiento',
      );
      return;
    }
    if (!/^[0-9]+$/.test(estacionamiento.precio)) {
      crearMensaje(
        'Debe ingresar el precio sin puntos ni guiones',
        'vEstacionamiento',
      );
      return;
    }
    if (estacionamiento.telefono.toString().length < 9) {
      crearMensaje('Debe ingresar un numero de 9 digitos', 'vEstacionamiento');
      return;
    }
    return true;
  };

  const publicarEstacionamiento = () => {
    if (checkValues() === true) {
      agregarEstacionamiento(estacionamiento);
    }
  };
  const changeEstacionamiento = () => {
    if (checkValues() === true) {
      editarEstacionamiento(estacionamiento);
    }
  };

  useEffect(() => {
    if (mensajeCreacion != null) {
      crearMensaje(mensajeCreacion, 'EstPublicado');
      setEstacionamiento({
        comuna: usuario.comuna,
        direccion: usuario.direccion,
        horaInicio: Date.now(),
        horaFin: Date.now(),
        telefono: '',
        precio: '',
        descripcion: '',
        tipoEstacionamiento: 1,
      });
    }
  }, [mensajeCreacion]);

  return {
    estacionamiento,
    setEstacionamiento,
    checkValues,
    publicarEstacionamiento,
    changeEstacionamiento,
  };
};

export default useStacionamiento;
