import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';

import {TouchableOpacity, Text, StyleSheet} from 'react-native';

export const NavigationButton = ({text, place, navigation, icon}) => {
  const goTo = () => {
    navigation.navigate(place);
  };
  return (
    <TouchableOpacity onPress={goTo} style={styles.navigationButton}>
      <Text style={styles.textoNavigation}>
        {text}  {icon && <Icon name={icon.name} size={icon.size} />}
      </Text>
    </TouchableOpacity>
  );
};

export const Button = ({text, onClick}) => {
  return (
    <TouchableOpacity onPress={onClick} style={styles.button}>
      <Text style={styles.texto}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 5,
    alignItems: 'center',
    padding: 15,
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom:30,
    backgroundColor: '#3b7ec0',
  },
  navigationButton: {
    borderRadius: 5,
    alignItems: 'center',
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
    padding: 18,
    marginBottom: 30,
    backgroundColor: '#FBFCFC',
  },
  textoNavigation: {color: '#8E44AD', fontSize: 18},
  texto: {color: 'white', fontSize: 20},
});
