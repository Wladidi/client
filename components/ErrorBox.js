import React from 'react';
import { View,Text, StyleSheet } from 'react-native'


const ErrorBox = ({mensaje}) => {
    return ( 
        <View style={styles.container}>
            <Text style={styles.text}>{mensaje}</Text>
        </View>
     );
}


const styles = StyleSheet.create({
    container:{
        width:'80%',
        backgroundColor:'#FF4C4C',
        marginLeft:'auto',
        marginRight:'auto',
        padding:10,
        borderRadius:3,
        marginBottom:20
    },
    text:{
        textAlign:'center'
    }
})
 
export default ErrorBox;