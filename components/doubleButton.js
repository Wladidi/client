import React, {useContext, useEffect, useState} from 'react';
import {Button, View, StyleSheet} from 'react-native';
import msgContext from '../context/mensajes/msgContext';

const DoubleButton = ({title, onClick, title2, onClick2}) => {
  const msjContext = useContext(msgContext);
  const [verificar, setVerificar] = useState(false);
  const {type} = msjContext;
  useEffect(() => {
    if (type === 'eliminarPublicacion') {
      setVerificar(true);
    } else setVerificar(false);
  }, [type]);

  return (
    <View style={styles.container}>
      <View style={styles.buttonContainer}>
        <Button title={title} onPress={onClick} />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          title={title2}
          onPress={onClick2}
          disabled={verificar}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
  },
});

export default DoubleButton;
