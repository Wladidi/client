import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const HeadBar = ({title}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        borderBottomColor:'#BFBFBF',
        borderBottomWidth:1,
        padding:15
    },
    title:{
        fontSize:25
    }
});

export default HeadBar;
