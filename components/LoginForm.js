import React, {useContext, useState, useEffect} from 'react';
import {Button} from '../components/Button';
import Styles from '../defaultStyles/styles';
import {useRef} from 'react';
import {Fumi} from 'react-native-textinput-effects';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import authContext from '../context/auth/authContext';
import msgContext from '../context/mensajes/msgContext';
import ErrorBox from './ErrorBox';
import {vLength, validate} from '../helpers/index';

const LoginForm = () => {
  const autContext = useContext(authContext);
  const {LogearUsuario, mensajeLogin} = autContext;
  const passwordref = useRef();
  const msjContext = useContext(msgContext);
  const {msg,type, crearMensaje} = msjContext;

  const [loginData, setData] = useState({email: '', password: ''});
  const {email, password} = loginData;

  const validateLogin = () => {
    if (vLength(email) || vLength(password)) {
      crearMensaje('Debe ingresar todos los campos','vForm');
      return;
    }
    if (!validate(email)) {
      crearMensaje('Debe ingresar un correo valido','vForm');
      return;
    }
    LogearUsuario(loginData);
  };
  useEffect(() => {
    if(mensajeLogin != null){
      crearMensaje(mensajeLogin.msg,'vForm');
    }
    
  }, [mensajeLogin]);
  return (
    <>
      <Fumi
        style={Styles.fumiStyle}
        label={'Correo'}
        iconClass={MaterialIcons}
        iconName={'email'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        autoFocus
        autoCompleteType={'email'}
        keyboardType="email-address"
        onChangeText={(text) => setData({...loginData, email: text})}
        onSubmitEditing={() => {
          passwordref.current.focus();
        }}
        blurOnSubmit={false}></Fumi>
      <Fumi
        style={Styles.fumiStyle}
        label={'Contraseña'}
        iconClass={MaterialIcons}
        iconName={'https'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        secureTextEntry
        onChangeText={(text) => setData({...loginData, password: text})}
        ref={passwordref}
      />
      {type==='vForm' && <ErrorBox mensaje={msg} />}

      <Button text="Iniciar Sesión" onClick={validateLogin} />
    </>
  );
};

export default LoginForm;
