import {Picker} from '@react-native-community/picker';
import React, {useContext, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Fumi} from 'react-native-textinput-effects';
import Entypo from 'react-native-vector-icons/Entypo';
import comunas from '../datos/comunas';
import Styles from '../defaultStyles/styles';
import {Button} from './Button';
import DateTimePicker from '@react-native-community/datetimepicker';
import msgContext from '../context/mensajes/msgContext';
import ErrorBox from './ErrorBox';
import SuccesMsgBox from './SuccesBox';
import useStacionamiento from '../hooks/useStacionamiento';

const EstacionamientoForm = () => {
  const msjContext = useContext(msgContext);
  const {msg, type} = msjContext;
  const [showReloj, setReloj] = useState({inicio: false, fin: false});

  const {
    estacionamiento,
    setEstacionamiento,
    checkValues,
    publicarEstacionamiento,
  } = useStacionamiento();

  const {
    comuna,
    direccion,
    tipoEstacionamiento,
    descripcion,
    telefono,
    precio,
  } = estacionamiento;

  const onChangeInicio = (event, selectedDate) => {
    setReloj({...showReloj, inicio: false});
    const currentDate = selectedDate || estacionamiento.horaInicio;
    setEstacionamiento({
      ...estacionamiento,
      horaInicio: currentDate,
    });
  };
  const onChangeFin = (event, selectedDate) => {
    setReloj({...showReloj, fin: false});
    const currentDate = selectedDate || estacionamiento.horaFin;
    setEstacionamiento({...estacionamiento, horaFin: currentDate});
  };
  const onPress = () => {
    if (checkValues() != true) return;
    publicarEstacionamiento();
  };

  return (
    <>
      <Text style={Styles.formTextEsta}>Comuna</Text>
      <Picker
        selectedValue={comuna}
        style={Styles.inputTextEsta}
        onValueChange={(itemValue) =>
          setEstacionamiento({...estacionamiento, comuna: itemValue})
        }>
        {comunas.map((com) => (
          <Picker.Item key={com} label={com} value={com} />
        ))}
      </Picker>
      <Fumi
        style={Styles.fumiEstaStyle}
        label={'Dirección *'}
        iconClass={Entypo}
        iconName={'location'}
        iconColor={'black'}
        value={direccion}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        onChangeText={(text) =>
          setEstacionamiento({...estacionamiento, ['direccion']: text})
        }></Fumi>
      <TouchableOpacity
        style={styles.button}
        onPress={() => setReloj({...showReloj, inicio: true})}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>
          Hora inicio disponible *
        </Text>
      </TouchableOpacity>
      {showReloj.inicio && (
        <DateTimePicker
          value={estacionamiento.horaInicio}
          mode={'time'}
          display="default"
          onChange={onChangeInicio}
        />
      )}
      <TouchableOpacity
        style={styles.button}
        onPress={() => setReloj({...showReloj, fin: true})}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>
          Hora fin disponibilidad *
        </Text>
      </TouchableOpacity>
      {showReloj.fin && (
        <DateTimePicker
          value={estacionamiento.horaFin}
          mode={'time'}
          display="default"
          onChange={onChangeFin}
        />
      )}
      <Fumi
        style={Styles.fumiEstaStyle}
        label={'Telefono Ej. 912345678 *'}
        iconClass={Entypo}
        iconName={'phone'}
        iconColor={'black'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        value={telefono}
        maxLength={9}
        returnKeyType="next"
        keyboardType="numeric"
        onChangeText={(text) =>
          setEstacionamiento({...estacionamiento, ['telefono']: text})
        }></Fumi>
      <Fumi
        style={Styles.fumiEstaStyle}
        label={'Precio por hora Ej. 5990 *'}
        iconClass={Entypo}
        iconName={'credit'}
        iconColor={'black'}
        maxLength={4}
        keyboardType="numeric"
        value={precio}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        onChangeText={(text) =>
          setEstacionamiento({...estacionamiento, ['precio']: text})
        }></Fumi>
      <Fumi
        style={Styles.fumiEstaStyle}
        label={'Descripcion'}
        iconClass={Entypo}
        iconName={'typing'}
        iconColor={'black'}
        iconSize={20}
        iconWidth={40}
        value={descripcion}
        inputPadding={16}
        returnKeyType="next"
        onChangeText={(text) =>
          setEstacionamiento({...estacionamiento, ['descripcion']: text})
        }></Fumi>
      <Text style={Styles.formTextEsta}>Tamaño del estacionamiento</Text>
      <Picker
        selectedValue={tipoEstacionamiento}
        style={Styles.inputTextEsta}
        onValueChange={(itemValue) => {
          setEstacionamiento({
            ...estacionamiento,
            ['tipoEstacionamiento']: itemValue,
          });
        }}>
        <Picker.Item label="Pequeño" value={1} />
        <Picker.Item label="Mediano" value={2} />
        <Picker.Item label="Grande" value={3} />
      </Picker>
      {type === 'EstPublicado' && <SuccesMsgBox mensaje={msg} />}
      {type === 'vEstacionamiento' && <ErrorBox mensaje={msg} />}
      <Button text="Publicar Estacionamiento" onClick={() => onPress()} />
    </>
  );
};
const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 16,
    width: '80%',
    marginRight: 'auto',
    marginBottom: 12,
    marginLeft: 'auto',
    backgroundColor: 'white',
  },
});

export default EstacionamientoForm;
