import React, {useContext} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import estacionamientoContext from '../context/estacionamientos/estacionamientoContext';
import {parseToTime} from '../helpers';
import DoubleButton from './doubleButton';

const Estacionamiento = ({estacionamiento, navigation}) => {
  const estContext = useContext(estacionamientoContext);
  const {eliminarEstacionamiento} = estContext;
  const {
    direccion,
    precio,
    comuna,
    telefono,
    horaInicio,
    horaFin,
    descripcion,
  } = estacionamiento;

  const deleteEstacionamiento = () => {
    eliminarEstacionamiento(estacionamiento._id);
  };
  const editEstacionamiento = () => {
    navigation.navigate('EditarEstacionamiento', {estac: estacionamiento});
  };

  const inicio = parseToTime(horaInicio);
  const fin = parseToTime(horaFin);
  return (
    <View style={styles.container}>
      <Text>Comuna: {comuna}</Text>
      <Text>Direccion: {direccion}</Text>
      <Text>Precio: ${precio}</Text>
      <Text>Telefono de contacto: {telefono}</Text>
      <Text>Hora de inicio : {inicio}</Text>
      <Text>Hora fin: {fin}</Text>
      {descripcion ? <Text>Descripcion: {descripcion}</Text> : null}
      <DoubleButton
        title="Editar"
        title2="Eliminar"
        onClick={() => editEstacionamiento()}
        onClick2={() => deleteEstacionamiento()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
    borderRadius: 7,
    backgroundColor: '#EBEBEB',
    marginBottom: 15,
    padding: 10,
  },
});

export default Estacionamiento;
