import React, {useContext, useEffect} from 'react';
import {Text} from 'react-native';
import estacionamientoContext from '../context/estacionamientos/estacionamientoContext';
import Estacionamiento from './Estacionamiento';

const ListadoMisEstacionamientos = ({navigation}) => {
  const estContext = useContext(estacionamientoContext);
  const {obtenerEstacionamientos, misEstacionamientos} = estContext;

  useEffect(() => {
    obtenerEstacionamientos();
  }, []);
  if (misEstacionamientos.length === 0)
    return (
      <Text style={{padding: 15}}>
        No hay Estacionamientos, comienza creando uno.
      </Text>
    );
  return (
    <>
      {misEstacionamientos.map((estacionamiento) => (
        <Estacionamiento
          key={estacionamiento._id}
          estacionamiento={estacionamiento}
          navigation={navigation}
        />
      ))}
    </>
  );
};

export default ListadoMisEstacionamientos;
