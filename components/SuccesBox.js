import React from 'react';
import { View,Text, StyleSheet } from 'react-native'


const SuccesMsgBox = ({mensaje}) => {
    return ( 
        <View style={styles.container}>
            <Text style={styles.text}>{mensaje}</Text>
        </View>
     );
}


const styles = StyleSheet.create({
    container:{
        width:'80%',
        backgroundColor:'#48C9B0',
        marginLeft:'auto',
        marginRight:'auto',
        padding:10,
        borderRadius:3,
        marginBottom:20,
        marginTop:20
    },
    text:{
        textAlign:'center'
    }
})
 
export default SuccesMsgBox;