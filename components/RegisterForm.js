import React, {useContext, useState, useEffect, useRef} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import ErrorBox from './ErrorBox';
import authContext from '../context/auth/authContext';
import {validate} from '../helpers/index';
import {Button} from './Button';
import Styles from '../defaultStyles/styles';
import {Picker} from '@react-native-community/picker';
import {validaRut, vLength, validarFecha} from '../helpers/index';
import {Fumi} from 'react-native-textinput-effects';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import msgContext from '../context/mensajes/msgContext';
import comunas from '../datos/comunas';
import DateTimePicker from '@react-native-community/datetimepicker';

const RegisterForm = () => {
  const autContext = useContext(authContext);
  const msjContext = useContext(msgContext);
  const {msg, type, crearMensaje} = msjContext;
  const ref1 = useRef();
  const ref2 = useRef();
  const ref3 = useRef();
  const ref4 = useRef();
  const ref5 = useRef();
  const {registrarUsuario, autenticado, mensajeRegistro} = autContext;
  const [show, setShow] = useState(false);
  const [usuario, setUsuario] = useState({
    rut: '',
    nombre: '',
    apellido: '',
    fechaNacimiento: Date.now(),
    password: '',
    email: '',
    comuna: 'Cerrillos',
    direccion: '',
    tipoCuenta: 1,
  });
  const {
    rut,
    nombre,
    apellido,
    fechaNacimiento,
    password,
    email,
    comuna,
    direccion,
    tipoCuenta,
  } = usuario;
  const onChange = (event, selectedDate) => {
    setShow(false);
    const currentDate = selectedDate || fechaNacimiento;
    setUsuario({...usuario, fechaNacimiento: currentDate});
  };
  const register = () => {
    if (
      vLength(rut) ||
      vLength(nombre) ||
      vLength(apellido) ||
      vLength(password) ||
      vLength(direccion)
    ) {
      crearMensaje('Debe ingresar todos los campos', 'vRegistrar');
      return;
    }
    if (!validaRut(rut)) {
      crearMensaje('Debe ingresar un rut valido');
      return;
    }
    if (password.trim().length < 6) {
      crearMensaje(
        'Debe ingresar una contraseña de al menos 6 caracteres',
        'vRegistrar',
      );
      return;
    }
    if (!validate(email)) {
      crearMensaje('Debe ingresar un correo valido');
      return;
    }
    if (!validarFecha(Date.parse(fechaNacimiento))) {
      crearMensaje(
        'Debes ser mayor de edad para crear una cuenta',
        'vRegistrar',
      );
      return;
    }
    registrarUsuario(usuario);
  };

  useEffect(() => {
    if (mensajeRegistro != null) {
      crearMensaje(mensajeRegistro.msg, 'vRegistrar');
    }
  }, [autenticado, mensajeRegistro]);
  return (
    <>
      <Fumi
        style={Styles.fumiStyle}
        label={'Rut Ej:12345678-9'}
        iconClass={MaterialIcons}
        iconName={'perm-identity'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        onChangeText={(text) => setUsuario({...usuario, rut: text})}
        onSubmitEditing={() => {
          ref1.current.focus();
        }}></Fumi>
      <Fumi
        style={Styles.fumiStyle}
        label={'Nombre'}
        iconClass={MaterialIcons}
        iconName={'contact-page'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        ref={ref1}
        onChangeText={(text) => setUsuario({...usuario, nombre: text})}
        onSubmitEditing={() => {
          ref2.current.focus();
        }}></Fumi>
      <Fumi
        style={Styles.fumiStyle}
        label={'Apellidos'}
        iconClass={MaterialIcons}
        iconName={'contact-page'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        ref={ref2}
        onSubmitEditing={() => {
          ref3.current.focus();
        }}
        onChangeText={(text) =>
          setUsuario({...usuario, apellido: text})
        }></Fumi>
      <TouchableOpacity style={styles.button} onPress={() => setShow(true)}>
        <Text style={{color: 'white'}}>Fecha de nacimiento</Text>
      </TouchableOpacity>
      {show && (
        <DateTimePicker
          value={fechaNacimiento}
          mode={'calendar'}
          display="default"
          onChange={onChange}
        />
      )}
      <Fumi
        style={Styles.fumiStyle}
        label={'Correo'}
        iconClass={MaterialIcons}
        iconName={'email'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        ref={ref4}
        onSubmitEditing={() => {
          ref5.current.focus();
        }}
        keyboardType="email-address"
        onChangeText={(text) => setUsuario({...usuario, email: text})}></Fumi>
      <Fumi
        style={Styles.fumiStyle}
        label={'Contraseña'}
        iconClass={MaterialIcons}
        iconName={'https'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        ref={ref3}
        secureTextEntry={true}
        onSubmitEditing={() => {
          ref4.current.focus();
        }}
        onChangeText={(text) =>
          setUsuario({...usuario, password: text})
        }></Fumi>

      <Text style={Styles.formText}>¿Cual es tu comuna?</Text>
      <Picker
        selectedValue={comuna}
        style={Styles.inputText}
        onValueChange={(itemValue) =>
          setUsuario({...usuario, comuna: itemValue})
        }>
        {comunas.map((com) => (
          <Picker.Item key={com} label={com} value={com} />
        ))}
      </Picker>
      <Fumi
        style={Styles.fumiStyle}
        label={'Dirección'}
        iconClass={MaterialIcons}
        iconName={'add-location'}
        iconColor={'#FFFFFF'}
        iconSize={20}
        iconWidth={40}
        inputPadding={16}
        returnKeyType="next"
        ref={ref5}
        onChangeText={(text) =>
          setUsuario({...usuario, direccion: text})
        }></Fumi>
      <Text style={Styles.formText}>¿Que deseas hacer con nosotros?</Text>
      <Picker
        selectedValue={tipoCuenta}
        style={Styles.inputText}
        onValueChange={(itemValue) =>
          setUsuario({...usuario, tipoCuenta: itemValue})
        }>
        <Picker.Item label="Quiero arrendar mi estacionamiento" value={1} />
        <Picker.Item label="Quiero arrendar estacionamientos" value={2} />
      </Picker>

      {type === 'vRegistrar' ? (
        <ErrorBox mensaje={msg} />
      ) : (
        <Button text="Registrarme" onClick={register} />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 16,
    width: '80%',
    marginRight: 'auto',
    marginBottom: 12,
    marginLeft: 'auto',
    backgroundColor: '#525353',
  },
});

export default RegisterForm;
