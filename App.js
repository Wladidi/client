import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Stacker from './drawer';
import AuthState from './context/auth/authState';
import MsgState from './context/mensajes/msgState';
import AsyncStorage from '@react-native-community/async-storage';
import tokenAuth from './config/token';
import EstacionamientoState from './context/estacionamientos/estacionamientoState';

const agregarToken = async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    tokenAuth(token);
  }
};
agregarToken();

const App = () => {
  return (
    <MsgState>
      <AuthState>
        <EstacionamientoState>
          <NavigationContainer>
            <Stacker />
          </NavigationContainer>
        </EstacionamientoState>
      </AuthState>
    </MsgState>
  );
};

export default App;
