export const validate = (text) => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    return false;
  } else {
    return true;
  }
};

export const validaRut = (rutCompleto) => {
    if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto)) return false;
    var tmp = rutCompleto.split('-');
    var digv = tmp[1];
    var rut = tmp[0];
    if (digv == 'K') digv = 'k';
    return dv(rut) == digv;
  },
  dv = (T) => {
    var M = 0,
      S = 1;
    for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
    return S ? S - 1 : 'k';
  };
export const vLength = (texto) => {
  if (texto.toString().trim().length === 0) {
    return true;
  } else return false;
};

export const validarFecha = (fecha) => {
  var d = new Date(fecha);
  var fechaHoy = new Date().getTime();
  var parsedDate = new Date(fechaHoy);
  const años = parsedDate.getFullYear() - d.getFullYear();
  if (años > 18) {
    return true;
  } else if (años === 18 && d.getMonth() + 1 < parsedDate.getMonth() + 1) {
    return true;
  } else if (
    d.getMonth() + 1 === parsedDate.getMonth() + 1 &&
    d.getDate() <= parsedDate.getDate()
  ) {
    return true;
  } else return false;
};

export const parseToTime = (date) =>{
  const hora = new Date(date);
  return hora.toLocaleTimeString().substr(0,5);
}