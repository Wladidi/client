import {
  CREAR_ESTACIONAMIENTO,
  OBTENER_MISESTACIONAMIENTOS,
  ESTACIONAMIENTO_ERROR,
  ELIMINAR_ESTACIONAMIENTO,
  SET_MENSAJE_ESTACIONAMIENTO,
} from '../../types/index';

export default (state, action) => {
  switch (action.type) {
    case CREAR_ESTACIONAMIENTO:
      return {
        ...state,
        misEstacionamientos: [
          ...state.misEstacionamientos,
          action.payload.estacionamientoCreado.estacionamiento,
        ],
        mensajeCreacion: action.payload.mensajeCreacion,
      };
    case OBTENER_MISESTACIONAMIENTOS:
      return {
        ...state,
        misEstacionamientos: action.payload,
      };
    case ESTACIONAMIENTO_ERROR:
      return {
        ...state,
        mensaje: action.payload,
      };
    case ELIMINAR_ESTACIONAMIENTO:
      return {
        ...state,
        misEstacionamientos: state.misEstacionamientos.filter(
          (estacionamiento) =>
            estacionamiento._id !== action.payload.idEstacionamiento,
        ),
        mensajeCorrecto: action.payload.msg,
      };
    case SET_MENSAJE_ESTACIONAMIENTO:
      return {
        ...state,
        mensajeCorrecto: null,
        mensajeCreacion: null,
      };
    default:
      return state;
  }
};
