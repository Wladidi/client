import React, {useReducer} from 'react';
import estacionamientoContext from './estacionamientoContext';
import estacionamientoReducer from './estacionamientoReducer';
import AxiosClient from '../../config/axios';
import {
  CREAR_ESTACIONAMIENTO,
  OBTENER_MISESTACIONAMIENTOS,
  EDITAR_ESTACIONAMIENTO,
  ESTACIONAMIENTO_ERROR,
  ELIMINAR_ESTACIONAMIENTO,
  SET_MENSAJE_ESTACIONAMIENTO,
} from '../../types/index';

const EstacionamientoState = (props) => {
  const initialState = {
    misEstacionamientos: [],
    mensajeCorrecto: null,
    mensaje: null,
    mensajeCreacion: null,
  };
  const [state, dispatch] = useReducer(estacionamientoReducer, initialState);

  const agregarEstacionamiento = async (estacionamiento) => {
    try {
      const resultado = await AxiosClient.post(
        '/api/estacionamiento',
        estacionamiento,
      );
      dispatch({
        type: CREAR_ESTACIONAMIENTO,
        payload: {
          estacionamientoCreado: resultado.data,
          mensajeCreacion: 'Estacionamiento publicado correctamente',
        },
      });
      setTimeout(() => {
        dispatch({
          type: SET_MENSAJE_ESTACIONAMIENTO,
        });
      }, 1000);
    } catch (error) {
      console.log(error);
      dispatch({
        type: ESTACIONAMIENTO_ERROR,
        payload: 'hubo un error',
      });
    }
  };

  const editarEstacionamiento = async (estacionamiento) => {
    try {
      const resultado = await AxiosClient.put(
        '/api/estacionamiento/',
        estacionamiento,
      );
      console.log(resultado.data)
    } catch (error) {
      console.log(error)
    }
  };
  const obtenerEstacionamientos = async () => {
    try {
      const resultado = await AxiosClient.get('/api/estacionamiento');
      dispatch({
        type: OBTENER_MISESTACIONAMIENTOS,
        payload: resultado.data.estacionamientos,
      });
    } catch (error) {
      dispatch({
        type: ESTACIONAMIENTO_ERROR,
        payload: 'hubo un error',
      });
    }
  };
  const eliminarEstacionamiento = async (idEstacionamiento) => {
    try {
      const response = await AxiosClient.delete(
        `/api/estacionamiento/${idEstacionamiento}`,
      );
      console.log(response.data);

      dispatch({
        type: ELIMINAR_ESTACIONAMIENTO,
        payload: {idEstacionamiento, msg: response.data.msg},
      });
      setTimeout(() => {
        dispatch({
          type: SET_MENSAJE_ESTACIONAMIENTO,
        });
      }, 1000);
    } catch (error) {
      console.log(error);
      dispatch({
        type: ESTACIONAMIENTO_ERROR,
        payload: 'Error al eliminar el estacionamiento',
      });
    }
  };
  return (
    <estacionamientoContext.Provider
      value={{
        misEstacionamientos: state.misEstacionamientos,
        mensajeCorrecto: state.mensajeCorrecto,
        mensajeCreacion: state.mensajeCreacion,
        agregarEstacionamiento,
        obtenerEstacionamientos,
        eliminarEstacionamiento,
        editarEstacionamiento
      }}>
      {props.children}
    </estacionamientoContext.Provider>
  );
};

export default EstacionamientoState;
