import {
  REGISTRO_EXITOSO,
  REGISTRO_ERROR,
  LOGIN_EXITOSO,
  LOGIN_ERROR,
  OBTENER_USUARIO,
  CERRAR_SESION,
} from '../../types/index';
import AsyncStorage from '@react-native-community/async-storage';

const deleteItem = async () => {
  try {
    await AsyncStorage.removeItem('token');
  } catch (error) {}
};

export default (state, action) => {
  switch (action.type) {
    case REGISTRO_EXITOSO:
      const storeData = async () => {
        try {
          await AsyncStorage.setItem('token', action.payload.token);
        } catch (e) {
          console.log(e);
        }
      };
      storeData();
      return {
        ...state,
        autenticado: true,
        mensajeRegistro: null,
        mensajeLogin: null,
        tipoCuenta: action.payload.tipoCuenta,
      };
    case OBTENER_USUARIO:
      return {
        ...state,
        autenticado: true,
        usuario: action.payload,
        tipoCuenta: action.payload.tipoCuenta,
      };
    case REGISTRO_ERROR:
      deleteItem();
      return {
        ...state,
        autenticado: false,
        token: null,
        mensajeRegistro: action.payload,
        tipoCuenta: null,
        usuario: null,
      };
    case LOGIN_EXITOSO:
      const storesData = async () => {
        try {
          await AsyncStorage.setItem('token', action.payload.token);
        } catch (e) {
          console.log(e);
        }
      };
      storesData();
      return {
        ...state,
        autenticado: true,
        mensaje: null,
        tipoCuenta: action.payload.tipoCuenta,
      };
    case LOGIN_ERROR:
      deleteItem();
      return {
        ...state,
        autenticado: false,
        token: null,
        mensajeLogin: action.payload,
        tipoCuenta: null,
        usuario: null,
      };
    case CERRAR_SESION:
      deleteItem();
      return {
        ...state,
        autenticado: false,
        token: null,
        tipoCuenta: null,
      };
    default:
      return state;
  }
};
