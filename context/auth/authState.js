import React, {useReducer} from 'react';
import authContext from './authContext';
import authReducer from './authReducer';
import {
  REGISTRO_EXITOSO,
  REGISTRO_ERROR,
  LOGIN_EXITOSO,
  LOGIN_ERROR,
  OBTENER_USUARIO,
  CERRAR_SESION,
} from '../../types/index';
import AsyncStorage from '@react-native-community/async-storage';
import AxiosClient from '../../config/axios';
import tokenAuth from '../../config/token';

const AuthState = (props) => {
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      return value;
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const initialState = {
    token: getData(),
    autenticado: null,
    usuario: null,
    mensajeRegistro: null,
    mensajeLogin: null,
    tipoCuenta: null,
  };
  const [state, dispatch] = useReducer(authReducer, initialState);

  //Funciones
  const registrarUsuario = async (datos) => {
    try {
      const respuesta = await AxiosClient.post('/api/user/create', datos, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
      dispatch({
        type: REGISTRO_EXITOSO,
        payload: respuesta.data,
      });
      usuarioAutenticado();
    } catch (error) {
      dispatch({
        type: REGISTRO_ERROR,
        payload: error.response.data,
      });
      setTimeout(() => {
        dispatch({
          type: REGISTRO_ERROR,
          payload: null,
        });
      }, 500);
    }
  };
  const usuarioAutenticado = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (token) {
        tokenAuth(token);
        try {
          const respuesta = await AxiosClient.get('/api/user/auth');
          dispatch({type: OBTENER_USUARIO, payload: respuesta.data.usuario});
        } catch (error) {
          dispatch({
            type: LOGIN_ERROR,
          });
        }
      }
    } catch (error) {
    }
  };
  const LogearUsuario = async (datos) => {
    try {
      const respuesta = await AxiosClient.post('/api/user/auth', datos);
      dispatch({
        type: LOGIN_EXITOSO,
        payload: respuesta.data,
      });
      usuarioAutenticado();
    } catch (error) {
      dispatch({
        type: LOGIN_ERROR,
        payload: error.response.data,
      });
      setTimeout(() => {
        dispatch({
          type: LOGIN_ERROR,
          payload: null,
        });
      }, 500);
    }
  };

  const CerrarSesion = () => {
    dispatch({
      type: CERRAR_SESION,
    });
  };

  return (
    <authContext.Provider
      value={{
        token: state.token,
        autenticado: state.autenticado,
        usuario: state.usuario,
        mensajeLogin: state.mensajeLogin,
        mensajeRegistro: state.mensajeRegistro,
        tipoCuenta: state.tipoCuenta,
        registrarUsuario,
        LogearUsuario,
        usuarioAutenticado,
        CerrarSesion,
      }}>
      {props.children}
    </authContext.Provider>
  );
};
export default AuthState;
