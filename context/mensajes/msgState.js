import React, {useReducer} from 'react';
import msgContext from './msgContext';
import msgReducer from './msgReducer';
import {CREAR_MSG, CERRAR_MSG} from '../../types/index';

const MsgState = (props) => {
  const initialState = {
    msg: null,
    type: null
  };

  const [state, dispatch] = useReducer(msgReducer, initialState);

  const crearMensaje = (msg,type) => {
    dispatch({
      type: CREAR_MSG,
      payload: {msg,type}
    });

    setTimeout(() => {
      dispatch({
        type: CERRAR_MSG,
      });
    }, 3000);
  };
  return (
    <msgContext.Provider value={{msg: state.msg,type: state.type, crearMensaje}}>
      {props.children}
    </msgContext.Provider>
  );
};

export default MsgState;
