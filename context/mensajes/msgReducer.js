import {CREAR_MSG, CERRAR_MSG} from '../../types/index';

export default (state, action) => {
  switch (action.type) {
    case CREAR_MSG:
      return {
        msg: action.payload.msg,
        type: action.payload.type
      };
    case CERRAR_MSG:
      return {
        msg: null,
        type: null
      };
    default:
      return state;
  }
};
