import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
  fumiStyle: {
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
    backgroundColor: '#525353',
    marginBottom: 10,
    color: '#FDFEFE',
  },
  fumiEstaStyle:{
    width: '80%',
    marginRight: 'auto',
    marginLeft: 'auto',
    backgroundColor: 'white',
    marginTop:5,
    marginBottom: 10,
  },
  inputText: {
    width: '80%',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 5,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom: 25,
    color: 'white',
  },
  inputTextEsta: {
    width: '80%',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 5,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginBottom: 25,
    color: 'black',
  },
  formText: {
    color: 'white',
    fontSize: 20,
    marginBottom: 15,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginTop:10
  },
  formTextEsta: {
    color: 'black',
    fontSize: 20,
    marginBottom: 15,
    marginRight: 'auto',
    marginLeft: 'auto',
    marginTop:10
  },
  container: {
    backgroundColor: '#18191A',
    height: '100%',
  },
  title: {
    fontSize: 40,
    color: 'white',
    textAlign: 'center',
    margin: 30,
  },
});

export default Styles;
