import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Start from '../screens/Start';
import Register from '../screens/Register';
import Login from '../screens/Login';
import authContext from '../context/auth/authContext';
import HomeArrendador from '../screens/HomeArrendador';
import HomeArrendatario from '../screens/HomeArrendatario';
import Icon from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import CrearEstacionamiento from '../screens/crearEstacionamiento';
import editarEstacionamiento from '../screens/editarEstacionamiento';
import Perfil from '../screens/Perfil';

const Stack = createStackNavigator();

function Stacker() {
  const autContext = useContext(authContext);
  const {autenticado, tipoCuenta} = autContext;

  return (
    <>
      <Stack.Navigator headerMode={'none'}>
        {autenticado ? (
          <>
            {tipoCuenta === 1 ? (
              <>
                <Stack.Screen name="Arrendador" component={TabArrendador} />
                <Stack.Screen
                  name="EditarEstacionamiento"
                  component={editarEstacionamiento}
                />
              </>
            ) : (
              <Stack.Screen name="Arrendatario" component={HomeArrendatario} />
            )}
          </>
        ) : (
          <>
            <Stack.Screen name="Start" component={Start} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Login" component={Login} />
          </>
        )}
      </Stack.Navigator>
    </>
  );
}
const Tab = createBottomTabNavigator();

const TabArrendador = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeArrendador"
      tabBarOptions={{
        activeTintColor: 'black',
        inactiveTintColor: 'grey',
        keyboardHidesTabBar: true,
      }}>
      <Tab.Screen
        name="HomeArrendador"
        component={HomeArrendador}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Icon name="home-sharp" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="CrearEstacionamiento"
        component={CrearEstacionamiento}
        options={{
          tabBarLabel: 'Publicar Estacionamiento',
          tabBarIcon: ({color, size}) => (
            <Icon name="add-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Perfil"
        component={Perfil}
        options={{
          tabBarLabel: 'Perfil',
          tabBarIcon: ({color, size}) => (
            <Icon name="person" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default Stacker;
