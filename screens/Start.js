import React, { useContext, useEffect } from 'react';
import {View, Text, StyleSheet, ImageBackground,StatusBar} from 'react-native';
import {NavigationButton} from '../components/Button';
import authContext from '../context/auth/authContext';
import background from '../img/Background.png';

const Start = ({navigation}) => {
  const autContext = useContext(authContext);
  const {usuarioAutenticado} = autContext;
  useEffect(() => {
    usuarioAutenticado()
  },[])
  return (
    <>
    <StatusBar backgroundColor='#552cc0'/>
    <ImageBackground style={styles.image} source={background}>
      <View>
        <Text style={styles.title}>Mi Estacionamiento</Text>
      </View>
      <View style={styles.container}>
        <NavigationButton
          text={'Iniciar Sesión'}
          place={'Login'}
          icon={{name:'login',size:18} }
          navigation={navigation}></NavigationButton>
        <NavigationButton
          text={'Registrarme'}
          place={'Register'}
          icon={{name:'form',size:18} }
          navigation={navigation}></NavigationButton>
      </View>
    </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  container: {height: '60%', justifyContent: 'center', alignItems: 'center'},
  title: {
    fontSize: 40,
    color: 'white',
    textAlign: 'center',
    margin: 30,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
});

export default Start;
