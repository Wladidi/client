import {Picker} from '@react-native-community/picker';
import React, {useContext, useEffect, useState} from 'react';
import {LogBox, Text} from 'react-native';
import {ScrollView, TouchableOpacity, StyleSheet} from 'react-native';
import {Fumi} from 'react-native-textinput-effects';
import HeadBar from '../components/HeadBar';
import comunas from '../datos/comunas';
import Styles from '../defaultStyles/styles';
import Entypo from 'react-native-vector-icons/Entypo';
import {Button} from '../components/Button';
import DateTimePicker from '@react-native-community/datetimepicker';
import useStacionamiento from '../hooks/useStacionamiento';
import msgContext from '../context/mensajes/msgContext';
import ErrorBox from '../components/ErrorBox';

LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
]);

const editarEstacionamiento = ({route}) => {
  const msjContext = useContext(msgContext);
  const {msg, type} = msjContext;
  const {estac} = route.params;
  estac.horaInicio = new Date(estac.horaInicio);
  estac.horaFin = new Date(estac.horaFin);

  const {
    estacionamiento,
    setEstacionamiento,
    checkValues,
    changeEstacionamiento,
  } = useStacionamiento();

  useEffect(() => {
    setEstacionamiento(estac);
  }, []);

  const [showReloj, setReloj] = useState({inicio: false, fin: false});

  const {
    comuna,
    direccion,
    telefono,
    precio,
    descripcion,
    tipoEstacionamiento,
    horaInicio,
    horaFin,
  } = estacionamiento;

  const onChangeInicio = (event, selectedDate) => {
    setReloj({...showReloj, inicio: false});
    const currentDate = selectedDate || horaInicio;
    setEstacionamiento({
      ...estacionamiento,
      horaInicio: currentDate,
    });
  };

  const onChangeFin = (event, selectedDate) => {
    setReloj({...showReloj, fin: false});
    const currentDate = selectedDate || horaFin;
    setEstacionamiento({...estacionamiento, horaFin: currentDate});
  };

  const guardarPublicacion = () => {
    if (checkValues() === true) {
      changeEstacionamiento();
    }
  };

  return (
    <>
      <HeadBar title="Editar publicación" />
      <ScrollView>
        <Text style={Styles.formTextEsta}>Comuna</Text>
        <Picker
          selectedValue={comuna}
          enabled={false}
          style={Styles.inputTextEsta}>
          {comunas.map((com) => (
            <Picker.Item key={com} label={com} value={com} />
          ))}
        </Picker>
        <Fumi
          style={Styles.fumiEstaStyle}
          editable={false}
          label={'Dirección'}
          iconClass={Entypo}
          iconName={'location'}
          iconColor={'black'}
          value={direccion}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          returnKeyType="next"
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => setReloj({...showReloj, inicio: true})}>
          <Text style={{color: 'black', fontWeight: 'bold'}}>
            Hora inicio disponible *
          </Text>
        </TouchableOpacity>
        {showReloj.inicio && (
          <DateTimePicker
            value={horaInicio}
            mode={'time'}
            display="default"
            onChange={onChangeInicio}
          />
        )}
        <TouchableOpacity
          style={styles.button}
          onPress={() => setReloj({...showReloj, fin: true})}>
          <Text style={{color: 'black', fontWeight: 'bold'}}>
            Hora fin disponibilidad *
          </Text>
        </TouchableOpacity>
        {showReloj.fin && (
          <DateTimePicker
            value={horaFin}
            mode={'time'}
            display="default"
            onChange={onChangeFin}
          />
        )}
        <Fumi
          style={Styles.fumiEstaStyle}
          label={'Telefono Ej. 912345678 *'}
          iconClass={Entypo}
          iconName={'phone'}
          iconColor={'black'}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          value={String(telefono)}
          maxLength={9}
          returnKeyType="next"
          keyboardType="numeric"
          onChangeText={(text) =>
            setEstacionamiento({...estacionamiento, ['telefono']: text})
          }
        />
        <Fumi
          style={Styles.fumiEstaStyle}
          label={'Precio por hora Ej. 5990 *'}
          iconClass={Entypo}
          iconName={'credit'}
          iconColor={'black'}
          editable={false}
          maxLength={4}
          keyboardType="numeric"
          value={String(precio)}
          iconSize={20}
          iconWidth={40}
          inputPadding={16}
          returnKeyType="next"
        />
        <Fumi
          style={Styles.fumiEstaStyle}
          label={'Descripcion'}
          iconClass={Entypo}
          iconName={'typing'}
          iconColor={'black'}
          iconSize={20}
          iconWidth={40}
          value={descripcion}
          inputPadding={16}
          returnKeyType="next"
          onChangeText={(text) =>
            setEstacionamiento({...estacionamiento, ['descripcion']: text})
          }
        />
        <Text style={Styles.formTextEsta}>Tamaño del estacionamiento</Text>
        <Picker
          enabled={false}
          selectedValue={tipoEstacionamiento}
          style={Styles.inputTextEsta}>
          <Picker.Item label="Pequeño" value={1} />
          <Picker.Item label="Mediano" value={2} />
          <Picker.Item label="Grande" value={3} />
        </Picker>
        {type === 'vEstacionamiento' && <ErrorBox mensaje={msg} />}
        <Button
          text="Editar mi publicacion"
          onClick={() => guardarPublicacion()}
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    padding: 16,
    width: '80%',
    marginRight: 'auto',
    marginBottom: 12,
    marginLeft: 'auto',
    backgroundColor: 'white',
  },
});

export default editarEstacionamiento;
