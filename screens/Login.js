import React from 'react';
import {View,Text, StatusBar, ScrollView, StyleSheet} from 'react-native';
import LoginForm from '../components/LoginForm';
import Styles from '../defaultStyles/styles';

const Login = () => {
  return (
    <>
      <StatusBar backgroundColor="#18191A" />
      <ScrollView style={Styles.container}>
        <Text style={Styles.title}>Mi Estacionamiento</Text>
        <View style={styles.container}>
          <LoginForm />
        </View>
      </ScrollView>
    </>
  );
};
const styles = StyleSheet.create({
  container: { justifyContent: 'center', alignItems: 'center',marginTop:90},
});

export default Login;
