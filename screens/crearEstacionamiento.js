import React from 'react';
import {ScrollView} from 'react-native';
import EstacionamientoForm from '../components/EstacionamientoForm';
import HeadBar from '../components/HeadBar';

const CrearEstacionamiento = () => {
  return (
    <>
      <HeadBar title="Publicar estacionamiento" />
      <ScrollView>
        <EstacionamientoForm />
      </ScrollView>
    </>
  );
};

export default CrearEstacionamiento;
