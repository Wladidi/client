import React, {useContext} from 'react';
import {ScrollView, StyleSheet, Text} from 'react-native';
import {Button} from '../components/Button';
import HeadBar from '../components/HeadBar';
import Title from '../components/Title';
import authContext from '../context/auth/authContext';

const Perfil = () => {
  const autContext = useContext(authContext);
  const {usuario, CerrarSesion} = autContext;
  return (
    <>
      <HeadBar title={`Perfil de ${usuario.nombre}`} />
      <ScrollView>
        <Title title="Tus datos" />
        <Text style={styles.text}>Rut: {usuario.rut}</Text>
        <Text style={styles.text}>Nombre: {usuario.nombre + " " + usuario.apellido}</Text>
        <Text style={styles.text}>Correo: {usuario.email}</Text>
        <Text style={styles.text}>Comuna: {usuario.comuna}</Text>
        <Text style={styles.text}>Dirección: {usuario.direccion}</Text>
        <Title title="¿Que deseas hacer?" />
        <Button
          text="Cerrar Sesion"
          onClick={() => {
            CerrarSesion();
          }}
        />
      </ScrollView>
    </>
  );
};


const styles = StyleSheet.create({
  text:{
      fontSize:15,
      paddingLeft:15
  }
})
export default Perfil;
