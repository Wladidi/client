import React, {useContext, useEffect, useState} from 'react';
import {RefreshControl, View, ScrollView} from 'react-native';
import ListadoMisEstacionamientos from '../components/ListadoMisEstacionamientos';
import HeadBar from '../components/HeadBar';
import SuccesMsgBox from '../components/SuccesBox';
import Title from '../components/Title';
import msgContext from '../context/mensajes/msgContext';
import authContext from '../context/auth/authContext';
import estacionamientoContext from '../context/estacionamientos/estacionamientoContext';

const HomeArrendador = ({navigation}) => {
  const autContext = useContext(authContext);
  const estContext = useContext(estacionamientoContext);
  const {mensajeCorrecto, obtenerEstacionamientos} = estContext;
  const {usuario} = autContext;
  const msjContext = useContext(msgContext);
  const {msg, type, crearMensaje} = msjContext;
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = () => {
    setRefreshing(true);
    obtenerEstacionamientos();
    setTimeout(() => {
      setRefreshing(false);
    }, 500);
  };

  useEffect(() => {
    if (mensajeCorrecto != null) {
      crearMensaje(mensajeCorrecto, 'eliminarPublicacion');
    }
  }, [mensajeCorrecto]);
  return (
    <View>
      <HeadBar title={`Bienvenido ${usuario.nombre}`} />
      <ScrollView
        style={{marginBottom: 60}}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <Title title={'Tus publicaciones'} />
        {type === 'eliminarPublicacion' && <SuccesMsgBox mensaje={msg} />}
        <ListadoMisEstacionamientos navigation={navigation} />
        <Title title={'Estacionamientos arrendados'} />
      </ScrollView>
    </View>
  );
};

export default HomeArrendador;
