import React from 'react';
import {ScrollView, Text, StatusBar} from 'react-native';
import RegisterForm from '../components/RegisterForm';
import Styles from '../defaultStyles/styles';

const Register = () => {
  return (
    <>
      <StatusBar backgroundColor="#18191A" />
      <ScrollView style={Styles.container}>
        <Text style={Styles.title}>¡Unete a nosotros!</Text>
        <RegisterForm />
      </ScrollView>
    </>
  );
};

export default Register;
