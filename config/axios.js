import axios from 'axios'
import Config from "react-native-config";

const AxiosClient = axios.create({
    baseURL: Config.URL_BASE
})

export default AxiosClient;